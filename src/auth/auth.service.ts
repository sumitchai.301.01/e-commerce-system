import { Injectable, UnauthorizedException, Inject, ExecutionContext} from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Request } from 'express';
import { Repository } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm';
import { Auth } from './entities/auth.entity';

@Injectable()
export class AuthService {
    
    constructor(
        @InjectRepository(Auth)
        private authRepository: Repository<Auth>,
        private userService: UsersService,
        private jwtService: JwtService,
        
    ) { }

    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.userService.findByEmail(email);
        if (user && (await bcrypt.compare(pass, user.password))) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }

    async signIn(
        username: string,
        password: string,
    ): Promise<{ access_token: string  }> {
        const user = await this.userService.findByEmail(username);
        const pass = await bcrypt.compare(password, user.password)
        if (pass !== true) {
            throw new UnauthorizedException();
        }
        const payload = { sub: user.id, username: user.email };

        let access_token = await this.jwtService.signAsync(payload)

        const authBeforCreate = {
            user_id: user.id,
            access_token: access_token
        }

        const toCreate = await this.authRepository.create(authBeforCreate)

        const authCreate = await this.authRepository.save(toCreate)

        let userAcces = {
            id: user.id,
            email: user.email,
            name: user.name,
            created_at: user.created_at,
            updated_at: user.updated_at,
            deleted_at: user.deleted_at,
        }

        let result = {
            status: true,
            message: "success",
            result: userAcces,
            access_token: access_token,
        }

        return result;

        // return pass;
    }

    async signOut(request: Request) {

        const [type, token] = request.headers.authorization?.split(' ') ?? []
        const access = type === 'Bearer' ? token : undefined

        const auth = await this.authRepository.softDelete({access_token: access})

        let result = {
            status: true,
            message: 'success',
        }

        return result;
    }


    async findOne(access_token: string) {
        const auth = await this.authRepository.findOneBy({access_token: access_token})
        let result = {
            status: true,
            message: 'success',
            auth: auth
        }
        
        return result;
    }
    
    async remove(request: Request){
        const [type, token] = request.headers.authorization?.split(' ') ?? []
        const access = type === 'Bearer' ? token : undefined

        const auth = await this.authRepository.softDelete({access_token: access})
        
        return true;
    }
}
