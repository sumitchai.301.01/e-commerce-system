import {
    CanActivate,
    ExecutionContext,
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { Request } from 'express';
import { Reflector } from '@nestjs/core';
import { AuthService } from './auth.service';


@Injectable()

export class AuthGuard implements CanActivate {
    constructor(
        private jwtService: JwtService,
        private reflector: Reflector,
        private readonly authService: AuthService
        
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isPublic = this.reflector.getAllAndOverride<boolean>('isPublic', [
            context.getHandler(),
            context.getClass(),
        ]);
        if (isPublic) {
            // 💡 See this condition
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const token = this.extractTokenFromHeader(request);

        const getAuth = await this.authService.findOne(token)
        
        if (!token) {
            throw new UnauthorizedException();
        }

        if(!getAuth.auth) {
            throw new UnauthorizedException();
        } else {
            try {
                const payload = await this.jwtService.verifyAsync(
                    token,
                    {
                        secret: jwtConstants.secret
                    }
                );
                // 💡 We're assigning the payload to the request object here
                // so that we can access it in our route handlers
                request['user'] = payload;
            } catch {
                const authRemove = await this.authService.remove(request)
                throw new UnauthorizedException();
            }
            return true;
        }
        
    }

    private extractTokenFromHeader(request: Request): string | undefined {
        const [type, token] = request.headers.authorization?.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}