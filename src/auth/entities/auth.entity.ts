import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, BeforeInsert, BeforeUpdate } from 'typeorm';

@Entity('auths')
export class Auth {
    @PrimaryGeneratedColumn()
    id: number

    @Column({
        type: 'nvarchar',
        length: 256,
        nullable: false,
    })
    access_token: string;

    @Column({
        nullable: false,
    })
    user_id: number;

    @CreateDateColumn({
        type: 'datetime',
    })
    created_at: Date

    @UpdateDateColumn({ 
        type: 'datetime' 
    })
    updated_at: Date

    @DeleteDateColumn({ 
        type: 'datetime',
        nullable: true
    })
    deleted_at: Date

    @BeforeInsert()
    protected setCreatedAt(): void {
        this.created_at = new Date();
    }

    @BeforeUpdate()
    protected setUpdatedAt(): void {
        this.updated_at = new Date();
    }

}
