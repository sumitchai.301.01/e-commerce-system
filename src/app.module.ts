import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { User } from './users/entities/user.entity';
import { ProductsModule } from './products/products.module';
import { CartsModule } from './carts/carts.module';
import { OrdersModule } from './orders/orders.module';
import { Product } from './products/entities/product.entity';
import { Cart } from './carts/entities/cart.entity';
import { Order } from './orders/entities/order.entity';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';
import { Auth } from './auth/entities/auth.entity';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'host.docker.internal',
            port: 3306,
            username: 'root',
            password: '',
            database: '',
            entities: [
                User,
                Product,
                Cart,
                Order,
                Auth
            ],
            synchronize: true,
        }),
        UsersModule,
        AuthModule,
        ProductsModule,
        CartsModule,
        OrdersModule,
    ],
    controllers: [AppController],
    providers: [AppService,
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        }
    ],
})
export class AppModule { }
