import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Repository } from 'typeorm';
import { CartsService } from 'src/carts/carts.service';
import { AuthService } from 'src/auth/auth.service';
import { Request } from 'express';

@Injectable()
export class OrdersService {
    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
        private cartService: CartsService,
        private authService: AuthService
    ) { }

    async create(createOrderDto: CreateOrderDto, request: Request) {
        const getCart = (await this.cartService.findOne(createOrderDto.cart_id)).result
        const [type, token] = request.headers.authorization?.split(' ') ?? []
        const access = type === 'Bearer' ? token : undefined
        const getAuth = (await this.authService.findOne(access)).auth
        const refNo = this.randomChar(3) + this.randomNumber(10)

        const BeforCreateOrder = {
            user_id: getAuth.user_id,
            cart_id: createOrderDto.cart_id,
            ref_no: refNo,
            cart_price_amount: getCart.product_price_amount
        }

        const toCreate = await this.orderRepository.create(BeforCreateOrder)
        const orderSave = await this.orderRepository.save(toCreate)
        const updateCartStatus = await this.cartService.updateOrderStatus(getCart.id, true)

        let result = {
            status: true,
            message: 'success',
            cart: toCreate
        }
        return result;
    }

    async findAll() {
        const getOrder = await this.orderRepository.find()
        // let i = 0
        // let l = getOrder.length
        // let g = []
        // const order = []

        // for(let key in getOrder){
        //     // g[key] =  getOrder[key].cart_id
        //     g[key] = (await this.cartService.findOne(getOrder[key].cart_id)).result

        //     order[key] = {
        //         ...getOrder[key],
        //         carts: g[key]
        //     }
            
        // }

        let result = {
            status: true,
            message: 'success',
            order: getOrder
        }
        return result;
    }

    async findOne(id: number) {
        const getOrder = await this.orderRepository.findOneBy({id: id})
        const getCart = await this.cartService.findOne(getOrder.cart_id)

        const orderDetail = {
            ...getOrder,
            cart: getCart.result
        }

        let result = {
            status: true,
            message: 'success',
            order: orderDetail
        }
        return result;
    }

    update(id: number, updateOrderDto: UpdateOrderDto) {
        return `This action updates a #${id} order`;
    }

    remove(id: number) {
        return `This action removes a #${id} order`;
    }

    private randomNumber(length) {
        let result = '';
        // const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const characters = '0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }

    private randomChar(length) {
        let result = '';
        // const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }
}
