import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, BeforeInsert, BeforeUpdate } from 'typeorm';

@Entity('carts')
export class Cart {
    @PrimaryGeneratedColumn()
    id: number

    @Column({
        nullable: true
    })
    user_id: number;

    @Column({
        nullable: true
    })
    product_id: number;

    @Column("decimal",{ 
        precision: 45,
        scale: 2,
        nullable: true 
    })
    product_price_amount: number;

    @Column({
        nullable: true
    })
    product_amount: number;

    @Column('boolean', {
        default: 0
    })
    order_status: boolean

    @CreateDateColumn({
        type: 'datetime',
    })
    created_at: Date

    @UpdateDateColumn({ 
        type: 'datetime' 
    })
    updated_at: Date

    @DeleteDateColumn({ 
        type: 'datetime',
        nullable: true
    })
    deleted_at: Date

    @BeforeInsert()
    protected setCreatedAt(): void {
        this.created_at = new Date();
    }

    @BeforeUpdate()
    protected setUpdatedAt(): void {
        this.updated_at = new Date();
    }
}
