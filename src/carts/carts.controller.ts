import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpStatus, Request } from '@nestjs/common';
import { CartsService } from './carts.service';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';

@Controller('api/carts')
export class CartsController {
  constructor(private readonly cartsService: CartsService) {}
  @HttpCode(HttpStatus.OK)
  @Post('select-product')
  create(@Request() req, @Body() createCartDto: CreateCartDto) {
    return this.cartsService.create(createCartDto, req);
  }

  @Get()
  findAll() {
    return this.cartsService.findAll();
  }

  @Get(':id' + '/detail')
  findOne(@Param('id') id: string) {
    return this.cartsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCartDto: UpdateCartDto) {
    return this.cartsService.update(+id, updateCartDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cartsService.remove(+id);
  }
}
