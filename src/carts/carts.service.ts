import { Injectable } from '@nestjs/common';
import { CreateCartDto } from './dto/create-cart.dto';
import { UpdateCartDto } from './dto/update-cart.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from './entities/cart.entity';
import { Repository } from 'typeorm';
import { ProductsService } from 'src/products/products.service';
import { Request } from 'express';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class CartsService {
    constructor(
        @InjectRepository(Cart)
        private cartRepository: Repository<Cart>,
        private productService: ProductsService,
        private authService: AuthService
    ) { }

    async create(createCartDto: CreateCartDto, request: Request) {
        const getProduct = await this.productService.findOne(createCartDto.product_id)
        const [type, token] = request.headers.authorization?.split(' ') ?? []
        const access = type === 'Bearer' ? token : undefined
        const getAuth = await this.authService.findOne(access)
        const productPriceAmount = getProduct.product.price * createCartDto.product_amount

        const BeforCreateCart = {
            user_id: getAuth.auth.user_id,
            product_id: createCartDto.product_id,
            product_amount: createCartDto.product_amount,
            product_price_amount: productPriceAmount
        }

        const createProduct = await this.cartRepository.create(BeforCreateCart)
        const toCreate = await this.cartRepository.save(createProduct)

        let result = {
            status: true,
            message: 'success',
            cart: toCreate
        }
        return result;
    }

    async findAll() {
        let cart = await this.cartRepository.find({order:{id: 'DESC'}})
        let result = {
            status: true,
            message: 'success',
            cart: cart
        }
        return result;
    }

    async findOne(id: number) {
        const cart = await this.cartRepository.findOneBy({id: id})
        const product = (await this.productService.findOne(cart.product_id)).product

        const productInCart = {
            ...cart,
            product: product
        }
        
        let result = {
            status: true,
            message: 'success',
            result: productInCart
        }
        return result;
    }

    update(id: number, updateCartDto: UpdateCartDto) {
        return `This action updates a #${id} cart`;
    }

    remove(id: number) {
        return `This action removes a #${id} cart`;
    }

    async updateOrderStatus(id: number, status: boolean){
        const cart = await this.cartRepository.findOneBy({id: id})
        const beforUpdateStatus = {
            order_status: status
        }

        const toUpdate = {
            ...cart,
            ...beforUpdateStatus
        }

        const updateStatus = await this.cartRepository.save(toUpdate)

        let result = {
            status: true,
            message: 'success'
        }

        return result;
    }
}
