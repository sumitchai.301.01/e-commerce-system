import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpStatus, Request, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Public } from 'src/auth/decorator/public.decorator';

@Controller('api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Public()
  @Post('register')
  @HttpCode(200)
  create(@Body() createUserDto: CreateUserDto) {

    let register = this.usersService.create(createUserDto)
    return register;

  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get('profile')
  findOne(@Request() req) {

    return this.usersService.findOne(+req.user.sub);
  }

  @Patch('profile/edit')
  update(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(+req.user.sub, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
