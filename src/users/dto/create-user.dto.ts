export class CreateUserDto {
    email: string;
    password: string;
    confirm_password: string;
    name: string;
    created_at: string;
    updated_at: string;
}
