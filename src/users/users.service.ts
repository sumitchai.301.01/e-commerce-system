import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) { }

    async create(createUserDto: CreateUserDto) {
        let result = {};
        if (createUserDto.password !== createUserDto.confirm_password) {

            result = {
                status: false,
                message: 'password not confirm'
            }

            return result;

        } else {
            const register = await this.usersRepository.create(createUserDto)
            const toCreate = await this.usersRepository.save(register)

            let user = {
                id: toCreate.id,
                email: toCreate.email,
                name: toCreate.name,
                created_at: toCreate.created_at,
                updated_at: toCreate.updated_at,
                deleted_at: toCreate.deleted_at
            }

            result = {
                status: true,
                message: 'success',
                user: user
            }

            return result;
        }
    }

    findAll() {
        return this.usersRepository.find();
    }

    async findOne(id: number) {
        const getUser = await this.usersRepository.findOneBy({ id: id })

        let user = {
            id: getUser.id,
            email: getUser.email,
            name: getUser.name,
            created_at: getUser.created_at,
            updated_at: getUser.updated_at,
            deleted_at: getUser.deleted_at
        }

        let result = {
            status: true,
            message: 'success',
            user: user
        }

        return result;

        // return user;
    }

    async update(id: number, updateUserDto: UpdateUserDto) {
        let result = {}

        let usersRepository = await this.usersRepository.findOneBy({ id: id });

        let user = {
            ...usersRepository,
            ...updateUserDto
        }

        const toUpdate = await this.usersRepository.save(user);

        let userUpdate = {
            id: toUpdate.id,
            email: toUpdate.email,
            name: toUpdate.name,
            created_at: toUpdate.created_at,
            updated_at: toUpdate.updated_at,
            deleted_at: toUpdate.deleted_at
        }

        result = {
            status: true,
            message: 'success',
            result: {
                user: userUpdate
            }
        }

        return result;
    }

    remove(id: number) {
        return `This action removes a #${id} user`;
    }

    findByEmail(email: string) {
        return this.usersRepository.findOneBy({ email });
    }
}
